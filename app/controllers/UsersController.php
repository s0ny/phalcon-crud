<?php

class UsersController extends ControllerBase
{

    public function indexAction()
    {
        $this->view->users = Users::find();
    }

    public function createAction()
    {
        if ($this->request->isPost()) {
            $users = new Users();
            if (!$users->save($this->request->getPost())) {
                foreach ($users->getMessages() as $error) {
                    $this->flash->error($error);
                }
            } else {
                $this->response->redirect('/users');
            }
        }

        $this->view->title = 'New data';
        $this->view->pick('users/form');
    }

    public function updateAction($id)
    {
        $user = Users::findFirst($id);
        if ($this->request->isPost()) {
            if (!$user->save($this->request->getPost())) {
                foreach ($user->getMessages() as $error) {
                    $this->flash->error($error);
                }
            } else {
                $this->response->redirect('/users');
            }
        }

        $this->tag->setDefaults($user->toArray());
        $this->view->title = 'Edit data';
        $this->view->pick('users/form');
    }

    public function deleteAction($id)
    {
        $user = Users::findFirst($id);

        if (!$user->delete()) {
            foreach ($user->getMessages() as $error) {
                $this->flash->error($error);
            }
        } else {
            $this->response->redirect('/users');
        }
    }
}
